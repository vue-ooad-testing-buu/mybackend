var express = require('express')
var router = express.Router()
var usersController = require('../controller/usersController')

/* GET users listing. */
router.get('/', usersController.getUsers)

router.get('/:id', usersController.getUser)

router.post('/', usersController.addUser)

router.put('/', usersController.updateUser)

router.delete('/:id', usersController.deleteUser)

module.exports = router
