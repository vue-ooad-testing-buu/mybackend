// mock data
const User = require('./../models/UserModel')

module.exports = {
  /* GET users listing. */
  async getUsers (req, res, next) {
    User.find()
      .then(data => {
        res.json(data)
      })
      .catch(err => {
        res.status(500).send(err)
      })
  },

  async getUser (req, res, next) {
    const id = req.params.id
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async addUser (req, res, next) {
    delete req.body._id
    const payload = req.body
    const user = new User(payload)
    user.save()
    res.json(user)
  },
  async updateUser (req, res, next) {
    const payload = req.body
    try {
      const user = await User.findOneAndUpdate(
        { _id: payload._id },
        { $set: payload }
      )
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    try {
      const user = await User.deleteOne({ _id: req.params.id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
